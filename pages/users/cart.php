<?php

require_once "../../config/dbConnection.php";

session_start();


// Define the data to be inserted
$data = [
    'product_name' => $_POST['product_name'],
    'user_id' => $_SESSION['id'],
    'quantity' => $_POST['quantity'],
    'amount' => $_POST['amount']
];

// Prepare the SQL query
$query = "INSERT INTO cart (product_name, user_id, quantity, amount) VALUES (?, ?, ?, ?)";
$statement = $con->prepare($query);

// Check if the prepare statement was successful
if (!$statement) {
    echo "Error preparing statement: " . $con->error;
    exit();
}

// Bind the values
$statement->bind_param('ssss', $data['product_name'], $data['user_id'], $data['quantity'], $data['amount']);

// Execute the query
$success = $statement->execute();


header('Location: ' . $_SERVER['HTTP_REFERER']);