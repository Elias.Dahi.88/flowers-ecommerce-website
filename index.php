<?php
session_start();

if(isset($_SESSION['id'])) {
    header("Location:./pages/users/index.php");
} else {
    header("Location:./pages/auth/login.html");
}

?>