<?php

require_once "../../config/dbConnection.php";

session_start();


// Define the data to be inserted
$data = [
    'name' => $_POST['name'],
    'email' => $_POST['email'],
    'number' => $_POST['number'],
    'message' => $_POST['message'],
    'user_id' => $_SESSION['id']
];

// Prepare the SQL query
$query = "INSERT INTO contacts (name, email, number, message, user_id) VALUES (?, ?, ?, ?, ?)";
$statement = $con->prepare($query);

// Check if the prepare statement was successful
if (!$statement) {
    echo "Error preparing statement: " . $con->error;
    exit();
}

// Bind the values
$statement->bind_param('sssss', $data['name'], $data['email'], $data['number'], $data['message'], $data['user_id']);

// Execute the query
$success = $statement->execute();


header("Location:index.php");