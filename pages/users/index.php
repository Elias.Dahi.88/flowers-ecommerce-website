<?php

require_once "../../config/dbConnection.php";


// Prepare the SQL query
$query = "SELECT * FROM categories";

// Execute the query
$result = $con->query($query);

$categories_content = "";

// Fetch the results
if($result) {
    while($row = mysqli_fetch_assoc($result)) {
        $categories_content .= <<<EOD
        <div class="box">
            <img src="../../images/{$row["image"]}" alt="">
            <div class="content">
                <h3>{$row['name']}</h3>
                <a href="../achburensis.html" class="btn">shop now</a>
            </div>
        </div>
        EOD;
    }
}


$content = <<<EOD
<!-- Swiper section starts  -->
<section class="home" id="home">
    <div class="swiper-container home-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <div class="box" style="background: url(../../images/pinkar.png);">
                    <div class="content">
                        <h3>share flowers and happines </h3>
                        <a href="../../NATURALFLOWER.html" class="btn">shop now</a>
                    </div>
                </div>
            </div>

            <div class="swiper-slide">
                <div class="box" style="background: url(../../images/ttt.jpg);">
                    <div class="content">
                        <h3>Send  <font color="#db7093"> flowers </font>like you means it</h3>
                        <a href="../../luxe.html" class="btn">shop now</a>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="box" style="background: url(../../images/pp.jpg);">
                    <div class="content">
                        <h3>  when we think of Perfume think of ours</h3>
                        <a href="../../PERFUMESECTION.html" class="btn">shop now</a>
                    </div>
                </div>

            </div>
            <div class="swiper-slide">
                <div class="box" style="background: url(../../images/eee.jpg);">
                    <div class="content">
                        <a href="../../PERFUMESECTION.html" class="btn">shop now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-pagination"></div>
</section>
<!-- Swiper section ends -->

<!-- banner section starts  -->
<section class="banner-container">
    <div class="banner">
        <img src="../../images/greenbar.jpg" alt="">
        <div class="content">
            <span><font style="color: white" >new arrivals</font></span>
            <h3><font style="color: white" >Flower bottle</font></h3>
            <a href="../../GPOTS.html" class="btn">shop now</a>
        </div>
    </div>
    <div class="banner">
        <img src="../../images/san.jpg" alt="">
        <div class="content">
            <span><font style="color: white" >new arrivals</font></span>
            <h3> <font style="color: white" >Perfume Gift</font></h3>
            <a href="../../PERFUMESECTION.html" class="btn">shop now</a>
        </div>
    </div>
</section>
<!-- banner section ends -->

<!-- category section starts  -->
<section class="category" id="category">

    <h1 class="heading"> shop by category </h1>

    <div class="box-container">
EOD.$categories_content.<<<EOD
    </div>

</section>
<!-- category section ends -->

<!-- Deal section starts  -->
<section class="icons-container">

    <div class="icon">
        <img src="../../images/icon1.png" alt="">
        <div class="content">
            <h3>free shipping</h3>
            <p>Free shipping on order</p>
        </div>
    </div>
    <div class="icon">
        <img src="../../images/icon2.png" alt="">
        <div class="content">
            <h3>100% Money Back</h3>
            <p>You’ve 30 days to Return</p>
        </div>
    </div>
    <div class="icon">
        <img src="../../images/icon3.png" alt="">
        <div class="content">
            <h3>Payment Secure</h3>
            <p>100% secure payment</p>
        </div>
    </div>
    <div class="icon">
        <img src="../../images/icon4.png" alt="">
        <div class="content">
            <h3>Support 24/7</h3>
            <p>Contact us anytime</p>
        </div>
    </div>

</section>
<!-- Deal section ends -->

<!-- contact section starts  -->
<section class="contact" id="contact">

    <h1 class="heading">get in touch</h1>

    <div class="row">

        <iframe class="map"  src= "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3375.6337969321007!2d35.271351075143144!3d32.21409697390384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151ce0ecb56421a3%3A0x9e7f39466322ba13!2sNablus!5e0!3m2!1sen!2s!4v1690637205940!5m2!1sen!2s"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>"

        <form action="./contact.php" method="POST">

            <div class="inputBox">
                <input name="name" type="text" required>
                <label>name</label>
            </div>
            <div class="inputBox">
                <input name="email" type="email" required>
                <label>email</label>
            </div>
            <div class="inputBox">
                <input name="number" type="number" required>
                <label>number</label>
            </div>
            <div class="inputBox">
                <textarea required name="message" id="" cols="30" rows="10"></textarea>
                <label>message</label>
            </div>

            <input type="submit" value="send_message" class="btn">

        </form>

    </div>

</section>
<!-- contact section ends -->
EOD;


include_once "./main-layout.php";

