<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rose Perfume Website</title>

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="../../css/user/style.css">

</head>
<body>
<script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

<!-- header section starts facebook,... -->
<header>

    <div class="header-1">

        <div class="share">
            <span> Find Us Here: </span>
            <a href="#" class="fab fa-facebook-f"> </a>
            <a href="#" class="fab fa-instagram"></a>
        </div>

        <div class="call">
            <span> Contact Us : </span>
            <a href="tel:0569758887">0569758887</a>
        </div>
        <div class="icons">
            <a href="../../CART.html" class="fas fa-shopping-cart"></a>
            <a href="../auth/login.html" class="fas fa-user-circle"></a>
        </div>

    </div>

    <div class="header-2">

        <a href="index.php" class="logo"> <i class="fas fa-band-aid"></i> Rose perfume </a>

        <form action="" class="search-bar-container">
            <input type="search" id="search-bar" placeholder="search ">
            <label for="search-bar" class="fas fa-search"></label>
        </form>

    </div>

    <div class="header-3">

        <div id="menu-bar" class="fas fa-bars"></div>
        <nav class="navbar">
            <ul>
                <li><b><a href="index.php" > <img src="../../images/home.png" style="width:25px;height:25px;"> Home</a></b></li>

                <li><b> <a href="#category"><img src="../../images/list.png" style="width:23px;height:23px;"> All Categories</a></b></li>

                <li> <b> <a href="#Plant"><img src="../../images/bouquet.png" style="width:25px;height:25px;">Flowers  <img src="../../images/down.png" style="width:10px;height:10px;"></a></b>
                    <ul class ="dropdown" style="width:180px; height:250px;">
                        <li><a style=" color: #DB7093FF">_________________</a></li>
                        <li><a href="../../NATURALFLOWER.html"> Natural flower</a></li>
                        <li><a href="../../auto.html">Automan Collection</a></li>
                        <li><a href="../../luxe.html">Luxe Collection</a></li>
                    </ul>
                </li>


                <li> <b><a href="#pots-packaging"><img src="../../images/medicine.png" style="width:25px;height:25px;">pots-packaging    <img src="../../images/down.png" style="width:10px;height:10px;"></a></b>
                    <ul class ="dropdown">
                        <li><a ></a></li>
                        <li><a style=" color: #DB7093FF">_________________</a></li>
                        <li><a href="../../MIXPACK.html">Mix packaging   </a></li>
                        <br>
                        <li><a href="../../packflower.html">packaging Flower </a></li>
                        <br>
                        <li><a href="../../GPOTS.html">glass pot </a></li>
                        <br>
                        <li><a href="../../PACKPERF.html">Packaging Perfume </a></li>
                        <br>
                        <li><a href="../../modern.html">Modern Pots</a></li>
                        <br>

                    </ul>
                </li>

                <li> <b><a href="../../PERFUMESECTION.html"> <img src="../../images/perfume(1).png" style="width:25px;height:25px;">Perfume Section</a></b></li>
                <li>  <b><a href="#GIFT"> <img src="../../images/gift.png" style="width:25px;height:25px;">Gifts <img src="../../images/down.png" style="width:10px;height:10px;"></a></b>
                    <ul class ="dropdown" style="width:180px; height:300px;" >
                        <li><a ></a></li>
                        <li><a style=" color: #DB7093FF">_________________</a></li>
                        <li><a href="../../wedding.html">wedding</a></li>
                        <br>
                        <li><a href="../../Valentine.html">Valentine's Day</a></li>
                        <br>
                        <li><a href="../../birthday.html">BirthDay</a></li>
                        <br>
                    </ul>

                </li>
                <li>  <b><a href="../../video.html"> <img src="../../images/customer.png" style="width:25px;height:25px;">Feadback Videos </a></b>

                <li> <b><a href="#contact"><img src="../../images/phonebook%20(2).png" style="width:25px;height:25px;">Contact</a></b></li>
            </ul>
        </nav>


    </div>

</header>
<!-- header section ends -->

<?php echo $content ?>

<!-- footer section starts  -->
<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3>about us</h3>
            <p>A website that helps you choose a variety of gifts of perfumes and flowers for all your happy occasions and provides the best items at various prices, Be happy. </p>
        </div>
        <div class="box">
            <h3>branch locations</h3>
            <a href="#">Jenin</a>
            <a href="#">Nablus</a>


        </div>


    </div>

    <h1 class="credit"> created by <span> Aya Awwad & Batool kittaneh</span> </h1>

</section>
<!-- footer section ends -->

<!-- scroll top button  -->
<a href="#home" class="scroll-top fas fa-angle-up"></a>


<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<!-- custom js file link  -->
<script src="../../script.js"></script>

</body>
</html>